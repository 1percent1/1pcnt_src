#!/usr/bin/env python3
# https://medium.com/@s.sadathosseini/real-time-apple-stock-prices-visualization-using-yfinance-and-streamlit-c4466d0a9b51
import time
import yfinance as yf

# Define the ticker symbol for Apple
ticker_symbol = 'AAPL'
# Get the data of the stock
apple_stock = yf.Ticker(ticker_symbol)
# Get the historical prices for Apple stock
historical_prices = apple_stock.history(period='1d', interval='1m')
# Get the latest price and time
latest_price = historical_prices['Close'].iloc[-1]
latest_time = historical_prices.index[-1].strftime('%H:%M:%S')
print(ticker_symbol, latest_time, latest_price)
