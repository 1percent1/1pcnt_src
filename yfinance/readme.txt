https://www.geeksforgeeks.org/get-financial-data-from-yahoo-finance-with-python/
import yfinance as yahooFinance

# Here We are getting Facebook financial information
# We need to pass FB as argument for that
GetFacebookInformation = yahooFinance.Ticker("META")

# whole python dictionary is printed here
print(GetFacebookInformation.info)

import yfinance as yahooFinance

GetFacebookInformation = yahooFinance.Ticker("META")

# Let us get historical stock prices for Facebook 
# covering the past few years.
# max->maximum number of daily prices available 
# for Facebook.
# Valid options are 1d, 5d, 1mo, 3mo, 6mo, 1y, 2y, 
# 5y, 10y and ytd.
print(GetFacebookInformation.history(period="max"))

import yfinance as yahooFinance
import pandas as pd

GetFacebookInformation = yahooFinance.Ticker("META")

pd.set_option('display.max_rows', None)
# Let us get historical stock prices for Facebook 
# covering the past few years.
# max->maximum number of daily prices available 
# for Facebook.
# Valid options are 1d, 5d, 1mo, 3mo, 6mo, 1y, 2y, 
# 5y, 10y and ytd.
print(GetFacebookInformation.history(period="max"))

import yfinance as yahooFinance

GetFacebookInformation = yahooFinance.Ticker("META")

# Valid options are 1d, 5d, 1mo, 3mo, 6mo, 1y,
# 2y, 5y, 10y and ytd.
print(GetFacebookInformation.history(period="6mo"))


https://medium.com/@kasperjuunge/yfinance-10-ways-to-get-stock-data-with-python-6677f49e8282

Fetch the most up-to-date stock price.

apple = yf.Ticker("AAPL")
print(apple.history(period="1d"))

Obtain adjusted data, which accounts for stock splits, dividends, etc.

data = yf.download("AAPL", start="2020-01-01", end="2021-01-01", auto_adjust=True)
print(data['Close'])  # This will show the adjusted close prices

recent_data = yf.download("AAPL", period="5d")
print(recent_data)

The download method is your go-to for obtaining historical data for any stock.

import yfinance as yf

data = yf.download("AAPL", start="2020-01-01", end="2021-01-01")
print(data.head()
