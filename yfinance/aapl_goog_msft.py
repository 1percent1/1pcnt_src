#!/usr/bin/env python3
import yfinance as yf

tickers = yf.Tickers('msft aapl goog')

# access each ticker using (example)
print(tickers.tickers['MSFT'].info)
print(tickers.tickers['AAPL'].history(period="1mo"))
print(tickers.tickers['GOOG'].actions)

