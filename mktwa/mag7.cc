// port python marketwatch ticker quote to c++20
// #!/usr/bin/env python3
// from marketwatch import MarketWatch
//
// username = 'evizbiz@gmail.com'
// password = 'HH2233hh'
// marketwatch = MarketWatch(username, password)
// ticks = ['aapl', 'amzn', 'googl', 'nvda', 'meta', 'msft', 'tsla']
// for tck in ticks:
//   p = marketwatch.get_price(tck)
//   print(tck, p)

// c++ template simplecli.cc -- https://github.com/yhirose/cpp-httplib.git
// Copyright (c) 2019 Yuji Hirose. All rights reserved. MIT License

#include <iostream>
#include <cassert>
// #include <format> // c++20 not fully available on linux mint 20
#include <string>

#include "httplib.h" // https://github.com/yhirose/cpp-httplib
using namespace std;

int hi_google(void) {
  auto host = "www.google.com/";
  auto http = "http://", https = "https://", port = ":8080";
#ifdef OPENSSL
  //auto scheme_host_port = "https://localhost:8080";
  //auto scheme_host_port = "https://google.com/search";
  //auto scheme_host_port = https + host + "advanced_search";
  //auto scheme_host_port = std::format("{}{}{}", https, host, "advanced_search");
  auto scheme_host_port = string(https) + string(host) + string("advanced_search");
#else
  //auto scheme_host_port = "http://localhost:8080";
  //auto scheme_host_port = "http://google.com/search";
  //auto scheme_host_port =  "http://www.google.com/advanced_search";
  //auto scheme_host_port = std::format("{}{}{}", http, host, "advanced_search");
  auto scheme_host_port = string(http) + string(host) + string("advanced_search");

#endif
  //cerr << scheme_host_port << endl;
  clog << scheme_host_port << endl;

  // if (auto res = httplib::Client(scheme_host_port).Get("/hi")) {
  if (auto res = httplib::Client(scheme_host_port).Get("/")) {
    cout << res->status << endl;
    cout << res->get_header_value("Content-Type") << endl;
    cout << res->body << endl;
  } else {
    cout << res.error() << endl;
  }

  return 0;
}

int mktw_tickers(void) {
  auto host = "www.marketwatch.com";
  auto http = "http://", https = "https://", port = ":8080";
#ifdef OPENSSL
  //auto scheme_host_port = "https://localhost:8080";
  //auto scheme_host_port = "https://google.com/search";
  auto scheme_host_port = string(http) + string(host);
#else
  //auto scheme_host_port = "http://localhost:8080";
  //auto scheme_host_port = "http://google.com/search";
  auto scheme_host_port = string(https) + string(host);
#endif
  //cerr << scheme_host_port << endl;
  clog << scheme_host_port << endl;

  // if (auto res = httplib::Client(scheme_host_port).Get("/hi")) {
  if (auto res = httplib::Client(scheme_host_port).Get("/")) {
    cout << res->status << endl;
    cout << res->get_header_value("Content-Type") << endl;
    cout << res->body << endl;
  } else {
    cout << res.error() << endl;
  }

  return 0;
}

int main(void) {
  //hi_google();
  mktw_tickers();
  return 0;
}
