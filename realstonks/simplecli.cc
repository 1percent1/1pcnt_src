//
//  simplecli.cc
//
//  Copyright (c) 2019 Yuji Hirose. All rights reserved.
//  MIT License
//

#include <iostream>
#include "httplib.h"

using namespace std;

int main(void) {
#ifdef OPENSSL
  //auto scheme_host_port = "https://localhost:8080";
  //auto scheme_host_port = "https://google.com/search";
  auto scheme_host_port =  "https://www.google.com/advanced_search";
#else
  //auto scheme_host_port = "http://localhost:8080";
  //auto scheme_host_port = "http://google.com/search";
  auto scheme_host_port =  "http://www.google.com/advanced_search";
#endif
  //cerr << scheme_host_port << endl;
  clog << scheme_host_port << endl;

  if (auto res = httplib::Client(scheme_host_port).Get("/hi")) {
    cout << res->status << endl;
    cout << res->get_header_value("Content-Type") << endl;
    cout << res->body << endl;
  } else {
    cout << res.error() << endl;
  }

  return 0;
}
