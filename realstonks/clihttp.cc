// #define CPPHTTPLIB_OPENSSL_SUPPORT
#include <cstdio>
#include <filesystem>
#include <print>
#include "httplib.h"

int main() {
  std::print("{0} {2}{1}!\n", "Hello", 23, "C++"); // overload (2)

  // HTTP
  httplib::Client cli("http://cpp-httplib-server.yhirose.repl.co");
  auto res = cli.Get("/hi");
  res->status;
  res->body;

  // HTTPS
  httplib::Client scli("https://cpp-httplib-server.yhirose.repl.co");
  auto sres = scli.Get("/hi");
  sres->status;
  sres->body;

  const auto tmp {std::filesystem::temp_directory_path() / "test.txt"};
  if( std::FILE* stream{std::fopen(tmp.c_str(), "w")} ) {
    std::print(stream, "File: {}", tmp.string()); // overload (1)
    std::fclose(stream);
  }
}
