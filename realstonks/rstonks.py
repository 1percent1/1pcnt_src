#!/usr/bin/env python3
import json, pprint, sys
from copy import deepcopy
from datetime import datetime, timedelta

try:
  import http.client
except:
  print('need http.client module...')
  sys.exit(1)

def connect(url = "realstonks.p.rapidapi.com", apikey = "d5642e65b0msh7690dce062731a6p1032fbjsn0d7344be61c6"):
  """
  use rapidapi realstonks api... basic/free
  see: https://rapidapi.com/amansharma2910/api/realstonks/pricing
  """
  conn = None
  header = { 'x-rapidapi-key': apikey, 'x-rapidapi-host': url }
  pprint.pp(header, indent=2)
  try:
    conn = http.client.HTTPSConnection(url)
  except Exception as e:
    print('connect realstonks rapidapi failed...', e)

  return conn, header

#def getreqs(conn, header, tickrs=['GOOGL','OARK', 'YMAX', 'SPYI', 'QQQI']):
def getreqs(conn, header, tickrs=['GOOGL','OARK', 'SPYI']):
  """
  seems QQQI and YMAX are not yet available?
  """
  datadict = {}
  for symb in tickrs:
    advanced = '/stocks/' + symb + '/advanced'
    print(advanced)
    try:
      conn.request('GET', advanced, headers=header)
      res = conn.getresponse()
      if(res):
        data = res.read() #; print(data)
        # datadict[symb] = deepcopy(data.decode('utf-8'))
        datadict[symb] = deepcopy(pprint.saferepr(data))
    except Exception as e:
      print('get request realstonks rapidapi failed...', symb, e)

  return datadict

def main(args):
  """
  seems QQQI is not yet available
  """
  # tickrs = ['GOOGL','OARK', 'YMAG', 'YMAX', 'SPYI', 'QQQI']
  tickrs = ['GOOGL','OARK', 'YMAX', 'SPYI']
  if(args): tickrs = args
  conn, header = connect()
  datadict = {}
  if(conn):
    datadict = getreqs(conn, header, tickrs)
    print(len(datadict))
  if( len(datadict) > 0 ):
    #pprint.pp(datadict, stream=None, indent=2, width=80, depth=None, compact=False, sort_dicts=True)
    pprint.pp(datadict, indent=2)
    now = datetime.now() ; timestamp = now.timestamp()  # Get timestamp (seconds since epoch)
    dt = datetime.fromtimestamp(timestamp)
    dt = str(dt).replace(' ', '_')
    filename = 'tickrs_' + dt + '.txt'
    with open(filename, mode="w") as file_obj:
      pprint.pp(datadict, indent=2, stream=file_obj)

if __name__ == "__main__":
  args = deepcopy(sys.argv[1:])
  main(args)

