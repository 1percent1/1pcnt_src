
# https://polygon.io/docs/stocks/getting-started

## Authentication

    Pass your API key in the query string like follows:

        https://api.polygon.io/v2/aggs/ticker/AAPL/range/1/day/2023-01-09/2023-01-09?apiKey=psU8AbdmwvxcPuP3PWxv38Fky6pjpipN

    Above REST returns a single line of JSON:

         '{"ticker":"AAPL","queryCount":1,"resultsCount":1,"adjusted":true,"results":[{"v":7.0790813e+07,"vw":131.6292,"o":130.465,"c":130.15,"h":133.41,"l":129.89,"t":1673240400000,"n":645365}],"status":"OK","request_id":"309e7d08a1af1b670710af4cdfbf72fb","count":1}'

    Alternatively, you can add an Authorization header to the request with your API Key as the token in the following form:

        Authorization: Bearer psU8AbdmwvxcPuP3PWxv38Fky6pjpipN

