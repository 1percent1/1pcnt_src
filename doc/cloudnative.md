
2021 cloud tech review and git updates:

TBD: libcloud container management vs VMs

Git:
https://docs.gitlab.com/ee/ci/examples/

git config --global user.email "evizbiz@gmail.com"
git config --global user.name "davidbhon"
cat .git/config
cat ~/.gitconfig
"""
[user]
        email = evizbiz@gmail.com
        name = davidbhon
"""

libcloud examples:
https://libcloud.readthedocs.io/en/latest/compute/examples.html

AWS Lambda:
chalice vs zappa

https://www.libhunt.com/compare-zappa--Zappa-vs-chalice
https://techbeacon.com/enterprise-it/serverless-development-evolves-why-my-team-uses-zappa

aws:
 curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
 unzip awscliv2.zip
 sudo ./aws/install
 https://console.aws.amazon.com/ec2globalview/home#
 https://console.aws.amazon.com/console/home?region=us-east-1
 https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-security-groups.html
 https://console.aws.amazon.com/iam/home#/security_credentials$access_key
 https://console.aws.amazon.com/kms/home?region=us-east-1#/kms/keys

 route53

 iam-mfa-googleauth-authkeys -- https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2
 hon_aws_multiregion    mrk-963d4317c618419a9125a504396ee230    Enabled SYMMETRIC_DEFAULT       Encrypt and decrypt

 https://console.aws.amazon.com/elasticbeanstalk/home?region=us-east-1#/gettingStarted
 ec2-with-sg
 s3,efs and dynamodb-mysql
 beanstalk-with-autoscale-autorestart
 cloud9
 cloudformation vs terraform
 lambda -- https://console.aws.amazon.com/lambda/home?region=us-east-1#/functions
 step-functions: https://aws.amazon.com/blogs/compute/introducing-the-amazon-eventbridge-service-integration-for-aws-step-functions/
 https://docs.aws.amazon.com/lambda/latest/dg/with-kinesis.html
 https://console.aws.amazon.com/kinesis/home?region=us-east-1#/home
