# 1percent

quantlib -- https://github.com/lballabio/QuantLib.git and https://pypi.org/project/QuantLib/

1percent.us -- python flask dash webapps for HonHedgeFund

12data.py and 12data.cc (try c++2023 new features)

Initialized with ...

Dash interactive table prototypes under ./wapps


## Modern C++

https://github.com/PacktPublishing/CPP-20-STL-Cookbook.git

https://github.com/yhirose/cpp-httplib.git

https://github.com/trungams/http-server.git


## Modern C

https://queue.acm.org/detail.cfm?id=3588242 -- C23 ANSI standard

https://github.com/nginx

## Free financial quotes

https://github.com/alpacahq and https://github.com/alpacahq/alpaca-py

https://www.google.com/finance/quote/UVXY:BATS?window=5D -- near realtime?

https://polygon.io/ ... https://github.com/pssolanki111/polygon.git

https://github.com/twelvedata/twelvedata-python

https://finance.yahoo.com/quote/WBA?p=WBA&.tsrc=fin-srch -- delayed 15min?

https://www.alphavantage.co/support/#api-key -- eviz.biz  realtime and delayed limited free  

## Crypto Mining Servers (for use with cpuminer)

https://github.com/JayDDee/cpuminer-opt.git

https://www.nicehash.com/algorithm

---

## Legacy prototypes (2020):

NGINX Ubuntu 18:
https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-ubuntu-18-04

nginx https gunicorn systemd:
sudo add-apt-repository ppa:certbot/certbot
sudo apt install python-certbot-nginx
sudo certbot --nginx -d 1percent.us -d www.1percent.us

---

12data.sh and 12data.py -- https://pypi.org/project/twelvedata/ ...  pip install twelvedata[pandas,matplotlib,plotly]

fmpcloud.py

ondemand_client.py -- barcharts

rapid_api.py

tda.py

yfinance_rt.py

yfi.py

---

and Submodules:

git submodule add https://github.com/twelvedata/twelvedata-python

git commit -m '12data repo submodule' .

git submodule add https://github.com/DataMining4Finance/yfinance

git commit -m 'yfinance repo submodule' .

