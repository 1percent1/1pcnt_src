#!/usr/bin/env python
try:
  # For Python 3.0 and later
  from urllib.request import urlopen
except ImportError:
  # Fall back to Python 2's urllib2
  from urllib2 import urlopen

import certifi, json

from pprint import pprint

_fmpkey = 'mVQRbJ7wDIQeEXsgSs9qgqKloqBAmXSD'

def get_jsonparsed_data(url):
  response = urlopen(url, cafile=certifi.where())
  data = response.read().decode("utf-8")
  return json.loads(data)

def price_info(tickr='AAPL'):
  endp = tickr+'?apikey=' + _fmpkey
  #his = 'https://financialmodelingprep.com/api/v3/historical-price-full/' + endp
  #fmp = get_jsonparsed_data(his) ; pprint(fmp)
  #qmin = 'https://financialmodelingprep.com/api/v3/quote-short/' + endp
  #fmp = get_jsonparsed_data(qfull) ; pprint(fmp)
  qfull = 'https://financialmodelingprep.com/api/v3/stock/full/real-time-price/' + endp
  fmp = get_jsonparsed_data(qfull) ; pprint(fmp)
  return qfull

def calendar(ickr='AAPL'):
  cal = 'https://financialmodelingprep.com/api/v3/stock_dividend_calendar?from=2023-10-10&to=2023-08-10&apikey='+_fmpkey
  pprint(get_jsonparsed_data(cal))

def pmp_fetch(tickrs=['aapl', 'googl', 'kqqq', 'qqqi', 'spyi', 'qdte', 'xdte', 'rdte'], quote_dict={}):
  for tickr in tickrs:
    q = price_info(tickr)
    quote_dict[tickr] = q

  return len(quote_dict)

def main():
  tickrs = ['aapl', 'googl', 'kqqq', 'qqqi', 'spyi', 'qdte', 'xdte', 'rdte']
  quote_dict = {}
  numquotes = pmp_fetch(tickrs, quote_dict)
  pprint(quote_dict)

if __name__ == '__main__':
  main()

