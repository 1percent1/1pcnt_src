#!/usr/bin/env python3
import requests

url = "https://latest-stock-price.p.rapidapi.com/price"

querystring = {"Indices":"<REQUIRED>"}

headers = {
	"X-RapidAPI-Key": "93e0b4a182msh69b5a7bf7ebfe85p1a919cjsnb1235f813606",
	"X-RapidAPI-Host": "latest-stock-price.p.rapidapi.com"
}

response = requests.get(url, headers=headers, params=querystring)

print(response.json())
