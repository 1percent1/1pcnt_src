#!/usr/bin/env python3
# thursday sept 9, 2021
# tuesday nov 21, 2023
from datetime import date
from pprint import pprint

def fsday(day=None):
  """
  Day date string can provide directory file-system-like item YYYY/MO/DA
  """
  if not day: day = date.today()
  day = str(day).replace('-','/')
  return day
# end fsday()

#def sum_assets(day=fsday(),asslist=[{},{},{},{}]):
#  """
#  asset list expects 4 accnt dicts --  2 IRA (Merrill Lynch and Fidelity) and 1 non-IRA (Zachstrade)
#  """
def sum_assets(day=fsday(),asslist=[]):
  """
  asset list expects accnt list--  2 IRA (Merrill Lynch and Fidelity) and 1 non-IRA (Zachstrade moved from TDA)
  """
  print("sum_assets> today:", day)
  # day = '2021/09/10'
  day = '2023/11/21'
  print("sum_assets> day override:", day)
  tot = 0.00
  for ass in asslist:
    tot += ass
  return tot
# end sum_assets()

def assets(today=fsday()):
  print("Today is:", today, "\n-----")
  day = today 
  day = '2024/01/01' # today
  print("Manual override day date:", day, today, "\n-----")

  fid_ira = { '2021/09/09':52405.35, '2021/09/10': 52261.76 }
  fid_ira = 36204.91 # nov 21, 2023
  print("Fidelity IRA assets:", fid_ira)

  ml_ira = { '2021/09/09':355440.92, '2021/09/10':351359.28 }
  # ml = { '2021/09/09':127135.48, '2021/09/10':127059.11 }
  ml_ira = 74755.02 # nov 21, 2023
  print("Merrill IRA assets:", ml_ira)
  
  # print("Total IRA:", fid_ira[day] + ml_ira[day])

  ztrade = -68000.00 # due to margin loan GOOGL and IEP 
  print("Total Non IRA assets:", ztrade)
  print("Total IRA:", fid_ira + ml_ira)

  tot_assets = [fid_ira, ml_ira, ztrade]
  tot = sum_assets(day, tot_assets)
  print("Total assets:", tot)

  return { today: tot_assets }
# end assets()

def dividends(divdict={'fid':0.0, 'ml':0.0, 'ztrade':0.0}):
  fid_jun = 70.00 + 17.40 + 24.33 + 26.00 + 20.00 + 97.80 + 50.00 + 17.60
  fid_jul = 66.00 + 17.40 + 26.64 + 20.00 + 97.80 + 17.60 + 62.00 + 10.00 + 38.00
  fid_aug = 70.00 + 17.40 + 25.06 + 24.00 + 20.00 + 97.80 + 18.00 + 45.00
  fid = fid_jun + fid_jul + fid_aug
  divdict['fid'] = fid / 3.0

  divdict['ztrade'] = 0.00 # due to $68k margin loan payments
  ztrade = divdict['ztrade']

  # moved tda to zackstrade aka ztrade
  # tda_jun = 117.75 + 20.20 + 260.00 + 820.0 + 25.70 + 165.00 + 193.93 + 360.00 + 220.70
  # tda_jul = 20.20 + 8.00 + 165.00 + 223.00 + 360.00 + 240.00 + 50.00 + 310.00
  # tda_aug = 20.20 + 8.00 + 187.86 + 165.00 + 360.00 + 335.00
  # tda_qtot = tda_jun + tda_jul + tda_aug
  # divdict['tda'] = tda_qtot / 3.0

  # ml_oct = 2060.62 ; ml_nov = 2335.84 ; ml_dec = 2272.67
  # ml_oct = 2014.98 ; ml_nov = 2374.88 ; ml_dec = 2307.69 # as of Nov 1, 2021

  # nov 2023 post condo purchase:
  ml_oct = 699.00 ; ml_nov = 388.31 ; ml_dec = 682.55
  ml = ml_oct + ml_nov + ml_dec
  divdict['ml'] = ml / 3.0

  pprint({'Monthly Dividends':divdict})

  qtot = fid + ml + ztrade
  mo_tot = qtot/3.0  # but most of ztrade dividend income is tax free munis.

  # fedtax = 0.20 * 0.6 * mo_tot # apply 20% fed tax to 60% of income ?
  fedtax = 0.20 * mo_tot # apply 20% fed tax to all of income ?

  print("\nTotal quarterly dividend:", qtot, "\nAvg monthly income:", mo_tot, "\nAssume approx tax witholding:", fedtax, " yields monthly dividend income:", mo_tot-fedtax)
  return mo_tot
# end dividends()

def ss_mdcare(ss=2422.40):
  # according to Nov 21, 2023 SSA.gov letter ...
  # mdcare = 148.50 + 59.40 + 12.30
  # mdcare = 297.00 # according to mysocialcecurity online benefits page 2021
  mdcare = 559.00 # according to my socialcecurity online benefits page nov 21, 2023
  ssmd =  ss - mdcare
  print("2024 Social Security before and after Medicare withholdings:", mdcare, ss, ssmd)
  return ssmd

def income(modiv, ssmd=ss_mdcare()):
  # parms are 2023 monthly before withholdings
  print("\nAnticipated total monthly dividend income total before fed tax witholdings:", modiv) 

  ssnet = 0.8 * ssmd
  print("\n2024 Monthly social security payments, after medicare and federal tax withholdings?", ssnet)

  mo_income = modiv + ssmd 
  print("\nAnticipated total monthly income total before witholdings:", mo_income) 

  # apply 20% fed tax to 60% of dividend income, assuming 40& of income is tax free muni-bonds ?
  fedtax = 0.20 * (ssmd + 0.6*modiv) 
  mo_income = mo_income - fedtax 
  print("\nAnticipated total monthly income total after witholdings (SS + divs):", mo_income) 

  return mo_income
# end income()

def main():
  print("My Social Security benefits: https://secure.ssa.gov/myCYB/start")
  print("Medicare premiums info: https://www.ssa.gov/benefits/medicare/medicare-premiums.html\n-----")

  all_assets = assets()
  print("Sum of All financial assets:", all_assets)

  divdict={'fid':0.0, 'ml':0.0, 'ztrade':0.0}
  modiv = dividends(divdict)
  print("Estimated total monthly dividend income before withholdings:", modiv)

  ss2021 = 2039.40
  ss2022 = 1.059 * ss2021 
  ss2024 = 2422.40
  ssmd = ss_mdcare(ss2024)
  monthly = income(modiv, ss2024)
  print("Estimated total monthly income net before and after withholdings:", ssmd+modiv, monthly)

if __name__ == '__main__':
  main()
