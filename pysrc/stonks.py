#!/usr/bin/env python3
import requests, sys
from pprint import pp
from datetime import date
from datetime import datetime as dtime
from copy import deepcopy as deepcp
from honhedge import portfolio # func provites dict lists of current and former positions/holdings

#def portfolio():
#  munis = ['bfk', 'ble', 'bta', 'cxe', 'dsm', 'mhd']
#  utils = ['am', 'mfd', 'dpg', ]
#  hiyld = ['ab', 'abr', 'acp', 'arr', 'atax', 'bld', 'bui', 'brw', 'bxmt','gain', 'gecc', 'ggn', 'glad', 'googl', 'vz']
#  all = munis + utils + hiyld
#  return all

#def pp2(*args):
def pp2(args={}):
  """
  prettyprint with indent and depth == 2
  """
  print("pp2 args:", args) 
  # pp(["pp2 *args param:", type(args), args], indent=2, depth=2)
  # pp2 =  pp.PrettyPrinter(indent=2m depth=2)
  # inspect.getargspec(pp2)
  #for a in args.keys():
  #  pp(a, indent=2, depth=2) # alias prettyprint func with 2 space indent
  pp(args)
  return len(args)

def stonks(tickrs=['gsbd', 'googl', 'iep', 'vz']):
  """
  https://rapidapi.com/amansharma2910/api/realstonks/pricing
  retuern dict of {ticker: json_quote }
  """
  header = {
	"X-RapidAPI-Key": "93e0b4a182msh69b5a7bf7ebfe85p1a919cjsnb1235f813606",
	"X-RapidAPI-Host": "realstonks.p.rapidapi.com"
  }
  # print(header)

  jquotes = {} 
  for tickr in tickrs:
    url = "https://realstonks.p.rapidapi.com/" + tickr
    tnow = dtime.now() ; print(tnow, url)
    resp = requests.get(url, headers=header) ; jresp = resp.json() 
    # hopefully jsresp contains timestamp (what TZ?)
    jquotes[tickr] = jresp #; print(jresp)

  return jquotes

def fetch_quotes(tickrs=['qqqi', 'spyi']):
  if( not tickrs ): tickrs = portfolio()
  print(date.today())
  tnow = dtime.now()
  jquotes = stonks(tickrs)
  for tickr in tickrs:
    print(tickr, jquotes[tickr])

  return jquotes

def main(args=['fbgrx', 'gsdb', 'qqqi', 'spyi']):
  """
  https://rapidapi.com/category/Finance -- try finance quote APIs listed by rapidAPI
  tax fee munis and taxable high yield CEFs
  """
  if not(args): args = deepcp(sys.argv[1:])
  print('main>', args)
  tickers = ['fbgrx', 'gsdb', 'qqqi', 'spyi'] 
  if( len(args) > 0 ):
    # tbd parse args for -cliopt and --cliopt=val val == key in portfolie holdings dict of ticker lists
    tickrs = args # assume each cli param is a ticker
  else:
    holdings = portfolio() # no args ... default list of holdings
    tickrs = holdings['all']

  pp2(tickrs)
  jquotes = fetch_quotes(tickrs)
  pp2(jquotes)

if __name__ == '__main__':
# args=['fbgrx', 'gsdb', 'qqqi', 'spyi']
  args = None
  main(args)

