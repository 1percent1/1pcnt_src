from googlefinance import googQuotes
import time, json
import os, sys

def quote(symbol):
  os.system('cls' if os.name=='nt' else 'clear')
  q = googQuotes(symbol)
  jq = json.dumps(q, indent=2)
  return jq

if __name__ == '__main__':
  tikr = 'uvxy'
  if len(sys.argv) > 1: tikr = sys.argv[1]
  jq = quote(tikr)
  print(jq)
