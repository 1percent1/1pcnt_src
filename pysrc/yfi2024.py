#!/usr/bin/env python3
import yfinance as yf

def info_tickr(tickr=None):
  """
  https://github.com/ranaroussi/yfinance
  """
  # get all stock info
  if(tickr): print(repr(tickr.info))

def allinfo_tickr(tickr=None):
  """
  https://github.com/ranaroussi/yfinance
  """
  # get all stock info
  print(repr(tickr.info))

  # get historical market data
  hist = tickr.history(period="1mo")

  # show meta information about the history (requires history() to be called first)
  print(tickr.history_metadata)

  # show actions (dividends, splits, capital gains)
  print(tickr.actions)
  print(tickr.dividends)
  print(tickr.splits)
  print(tickr.capital_gains)  # only for mutual funds & etfs

  # show share count
  print(tickr.get_shares_full(start="2022-01-01", end=None))

  # show financials:
  # - income statement
  print(tickr.income_stmt)
  print(tickr.quarterly_income_stmt)
  # - balance sheet
  print(tickr.balance_sheet)
  print(tickr.quarterly_balance_sheet)
  # - cash flow statement
  print(tickr.cashflow)
  print(tickr.quarterly_cashflow)
  # see `Ticker.get_income_stmt()` for more options

  # show holders
  print(tickr.major_holders)
  print(tickr.institutional_holders)
  print(tickr.mutualfund_holders)
  print(tickr.insider_transactions)
  print(tickr.insider_purchases)
  print(tickr.insider_roster_holders)

  print(tickr.sustainability)

  # show recommendations
  print(tickr.recommendations)
  print(tickr.recommendations_summary)
  print(tickr.upgrades_downgrades)

  # Show future and historic earnings dates, returns at most next 4 quarters and last 8 quarters by default.
  # Note: If more are needed use tickr.get_earnings_dates(limit=XX) with increased limit argument.
  print(tickr.earnings_dates)

  # show ISIN code - *experimental*
  # ISIN = International Securities Identification Number
  print(tickr.isin)

  # show options expirations
  print(tickr.options)

  # show news
  print(tickr.news)

  # get option chain for specific expiration
  print('opt = tickr.option_chain("YYYY-MM-DD")')
  print('data available via: opt.calls, opt.puts')

def main(args='googl gooy ifn svol tltw ymac ymax'):
  print('yfinance fetch ticker info:', args)
  #tickers = yf.Tickers('msft aapl googl')
  tickers = yf.Tickers(args)
  for tickr in tickers.tickers:
    print(tickr)
    info_tickr(tickr)

if(__name__ == '__main__'):
  """
  https://github.com/ranaroussi/yfinance
  """
  #msft = yf.Ticker('MSFT')
  #msft = tickers.tickers['MSFT']
  #msft.info
  #aapl = tickers.tickers['AAPL']
  #aapl.history(period="1mo")
  #goog = tickers.tickers['GOOG']
  #goog.actions
  #main(args='googl gooy ifn svol tltw ymac ymax')
  main(args='googl gooy')

