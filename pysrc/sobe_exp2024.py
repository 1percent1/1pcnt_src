#!/usr/bin/env python3
import sys

def print_precision(prefix, x, n=2):
  sx = '{:.{}f}'.format(x, n)
  res = prefix + ': ' + sx ; print(res)
  return res

def inc_exp_mo(postax = 0.8):
  ss = 1639.0 # after witholdings
  fid = 225.87 # 2nd qtr 2024
  fid = postax*fid
  ml = 370.78 # (433.31 + 219.84 + 459.21) /3.0 # 2024 estimated qtr oct,bov,dec
  ml = postax*ml
  z = 567.79 # 3974.56 / 7 ... 2024 ytd jan - jul
  z = postax*z
  div = fid + ml + z
  print('post witholdings inc: ss,fid,ml,zacks', ss, fid, ml, z)
  inc = ss + div
  print('net inc:', ss, '+', div, '==', inc)
  car = 358+151 # loan plus insurance
  sobe = 635+150+80+357 # hoa + utils + prop tax
  exp = car + sobe
  print('min expenses sans food and bev and fun: sobe + car ==', exp)
  net = inc - exp
  print('net: inc - exp ==', net)
  print('max daily budget (food and fun) ==', net/31.0)

def main(args=None):
  postax = 0.8
  if(args): postax = float(args)
  inc_exp_mo(postax)

if __name__ == '__main__':
  if(len(sys.argv) > 1):
    main(sys.argv[1])
  else:
    main()

