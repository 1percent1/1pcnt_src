#!/usr/bin/env python3

import sys
from copy import deepcopy

# https://github.com/wesinator/pynslookup
from nslookup import Nslookup as dns

_google_dns = ["8.8.8.8", "8.8.4.4"]
_dns_servers = ["1.1.1.1"] # local host dns
_dns_servers.append(_google_dns)
_dns_query = dns() # Initialize Nslookup
# _dns_query = dns(dns_servers=_dns_servers, verbose=False, tcp=False)

def nslookup(names=['eviz.biz', '4science.be', '90ghz.org']):
  """
  Nslookup constructor supports optional arguments for setting custom dns servers (defaults to system DNS),
  verbosity (default: True) and using TCP instead of UDP (default: False)
  """
  global _dns_query
  domain = "example.com"
  if not(names): names = [domain]
  for domain in names:
    ips_record = _dns_query.dns_lookup(domain) ; soa_record = _dns_query.soa_lookup(domain)
    print(ips_record.response_full, ips_record.answer)
    print(soa_record.response_full, soa_record.answer)
  # end forloop
# end nslookup func

def main(args):

  if not(args): args = ['eviz.biz', '4science.be', '90ghz.org']
  nslookup(args)

if __name__ == '__main__':
  args = None
  if len(sys.argv) > 1: args = deepcopy(sys.argv[1:])
  main(args)

