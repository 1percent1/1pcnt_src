import requests

url = "https://stock-analysis.p.rapidapi.com/api/v1/resources/earnings-history"

querystring = {"ticker":"AAPL"}

headers = {
	"X-RapidAPI-Key": "93e0b4a182msh69b5a7bf7ebfe85p1a919cjsnb1235f813606",
	"X-RapidAPI-Host": "stock-analysis.p.rapidapi.com"
}

response = requests.get(url, headers=headers, params=querystring)

print(response.json())
