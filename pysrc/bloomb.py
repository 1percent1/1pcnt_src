import requests

url = "https://bb-finance.p.rapidapi.com/stock/get-financials"

querystring = {"id":"aapl:us"}

headers = {
	"X-RapidAPI-Key": "93e0b4a182msh69b5a7bf7ebfe85p1a919cjsnb1235f813606",
	"X-RapidAPI-Host": "bb-finance.p.rapidapi.com"
}

response = requests.get(url, headers=headers, params=querystring)

print(response.json())

##################

import requests

url = "https://bb-finance.p.rapidapi.com/stock/get-statistics"

querystring = {"id":"aapl:us","template":"STOCK"}

headers = {
	"X-RapidAPI-Key": "93e0b4a182msh69b5a7bf7ebfe85p1a919cjsnb1235f813606",
	"X-RapidAPI-Host": "bb-finance.p.rapidapi.com"
}

response = requests.get(url, headers=headers, params=querystring)

print(response.json())
