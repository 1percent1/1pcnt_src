#!/usr/bin/env python3
import requests, sys
from pprint import pp
from datetime import date
from datetime import datetime as dtime
from copy import deepcopy as deepcp
from honhedge import pp2, portfolio # func provites dict lists of current and former positions/holdings

def alphavantage_quotes(tickrs=['gsbd', 'qqqi', 'spyi']):
  if( not tickrs ): tickrs = portfolio()
  print(date.today())
  tnow = dtime.now()
  jquotes = {}
  # from https://www.alphavantage.co/documentation/
  # url = 'https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=IBM&interval=5min&apikey=demo'
  # url = 'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=IBM&apikey=demo'
  # key = 'demo' 
  key = 'YDQ7G17XAY7DH4BQ' 
  for tickr in tickrs:
    url = 'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=' + tickr + '&apikey=' + key
    print(url)
    q = requests.get(url)
    jquotes[tickr] = jq = q.json()
    print(tickr, jq)

  return jquotes

def main(args=['fbgrx', 'gsdb', 'qqqi', 'spyi']):
  """
  tax fee munis and taxable high yield CEFs
  https://www.alphavantage.co/academy/
  https://www.alphavantage.co/support/#api-key
  Welcome to Alpha Vantage! Your dedicated access key is: YDQ7G17XAY7DH4BQ.
  """
  if not(args): args = deepcp(sys.argv[1:])
  print('main>', args)
  tickers = ['fbgrx', 'gsdb', 'qqqi', 'spyi'] 
  if( len(args) > 0 ):
    # tbd parse args for -cliopt and --cliopt=val val == key in portfolie holdings dict of ticker lists
    tickrs = args # assume each cli param is a ticker
  else:
    holdings = portfolio() # no args ... default list of holdings
    tickrs = holdings['all']

  pp2(tickrs)
  jquotes = alphavantage_quotes(tickrs)
  pp2(jquotes)

if __name__ == '__main__':
  # args=['fbgrx', 'gsdb', 'qqqi', 'spyi']
  args = None
  main(args)

