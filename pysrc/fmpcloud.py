#!/usr/bin/env python
from pyfmpcloud import settings
from pyfmpcloud import stock_time_series as sts
import os, sys

fmpc_key = '59905dc223dae03bb6d76fc0c201433c'
settings.set_apikey(fmpc_key)

def rtquote(tkr='uvxy,mux,goro'):
  rtq = sts.real_time_quote(tkr)
  return rtq

if __name__ == '__main__':
  tkr = 'uvxy,goro,mux'
  if len(sys.argv) > 1: tkr = sys.argv[1]
  rtq = rtquote(tkr)
  print(rtq)

