#!/usr/bin/env python3
# v0.0 saturday nov 25, 2023

import inspect
from pprint import pp
from datetime import date

def pp2(*args):
  """
  prettyprint with indent and depth == 2
  """
  # pp(["pp2 *args param:", type(args), args], indent=2, depth=2)
  # pp2 =  pp.PrettyPrinter(indent=2m depth=2)
  # inspect.getargspec(pp2)
  for a in args: 
    pp(a, indent=2, depth=2) # alias prettyprint func with 2 space indent

def fsday(day='2023/11/29'):
  """
  Day date string can provide directory file-system-like item YYYY/MO/DA
  """
  if not day:
    day = date.today()
    day = str(day).replace('-','/')

  return day
# end fsday()

def sum_assets(day=fsday(),asslist=[{},{},{},{}]):
  """
  asset list expects 4 accnt dicts --  2 IRA (Merrill Lynch and Fidelity) and 2 non-IRAs (Merrill and TDA)
  """
  pp2("sum_assets> today:", day)
  tot = 0.00
  for ass in asslist:
    tot += ass[day]
  return tot
# end sum_assets()

def assets(day=fsday()):
  # manual edit -- honhedgefund closing day='2023/11/29'
  pp2("honhedgefund closing date:", day)

  #fid_ira = { '2021/09/09':52405.35, '2021/09/10': 52261.76 }
  fid_ira = { day:36320.48 } # pp2("Fidelity IRA assets:", fid_ira)

  # ml_ira = { '2021/09/09':355440.92, '2021/09/10':351359.28 }
  ml_ira = { day:75606.54 } # pp2("Merrill IRA assets:", ml_ira)

  # ml = { '2021/09/09':127135.48, '2021/09/10':127059.11 }
  # tda = { '2021/09/09':170943.10, '2021/09/10':170055.33 }
  # pp2("ML and TDA nonIRA assets:", tml, da) # ML all spent, and TDA transferred to Zackstrade

  zt = { day:100279.27 } # Zackstrade
  pp2("Total Non IRA aka Zackstrade", zt)
  pp2("Total Fidelity and Merrill Lynch IRAs:", fid_ira[day] + ml_ira[day])

  tot_assets = [fid_ira, ml_ira, zt]
  tot = sum_assets(day, tot_assets)
  pp2("Total assets:", tot)

  return { day: tot_assets }
# end assets()

def ira_dividends(divdict={'fid':0.0, 'ml':0.0}):
  """
  return IRA dividend income assuming 20% fed tax
  """
  fid_jun = 70.00 + 17.40 + 24.33 + 26.00 + 20.00 + 97.80 + 50.00 + 17.60
  fid_jul = 66.00 + 17.40 + 26.64 + 20.00 + 97.80 + 17.60 + 62.00 + 10.00 + 38.00
  fid_aug = 70.00 + 17.40 + 25.06 + 24.00 + 20.00 + 97.80 + 18.00 + 45.00
  fid_qtot = fid_jun + fid_jul + fid_aug
  divdict['fid'] = fid_qtot / 3.0

  # ml_oct = 2060.62 ; ml_nov = 2335.84 ; ml_dec = 2272.67
  ml_oct = 2014.98 ; ml_nov = 2374.88 ; ml_dec = 2307.69 # as of Nov 1, 2021
  ml_qtot = ml_oct + ml_nov + ml_dec
  divdict['ml'] = ml_qtot / 3.0
  pp2({'Monthly Dividends':divdict})

  qtot_ira = fid_qtot + ml_qtot
  mo_ira = qtot_ira/3.0
  fedtax = 0.20 * mo_ira # app2ly 20% fed tax to ira income
  net_ira = mo_ira - fedtax
  pp2("\nNet IRA quarterly dividend after taxes:", qtot_ira, "\nAvg monthly income:", mo_ira, "\nAssume app2rox tax witholding:", fedtax, " yields monthly dividend income:", net_ira)
  return net_ira
# end ira_dividends()

def nonira_dividends(day=fsday()):
  # TDA assets moved to Zackstrade for lower margin rates...
  # tda_jun = 117.75 + 20.20 + 260.00 + 820.0 + 25.70 + 165.00 + 193.93 + 360.00 + 220.70
  # tda_jul = 20.20 + 8.00 + 165.00 + 223.00 + 360.00 + 240.00 + 50.00 + 310.00
  # tda_aug = 20.20 + 8.00 + 187.86 + 165.00 + 360.00 + 335.00
  # tda_qtot = tda_jun + tda_jul + tda_aug
  # divdict['tda'] = tda_qtot / 3.0
  # TDA accnt moved to Zackstrad

  # mostly tax free munibond CEFs but also IEP
  ztmuni = 0.0 # due to margin loan ... but otherwise approx $700/mo
  zt = 0.0

  nonira_net = 0.8*zt + ztmuni
  pp2("\nMonthly IRA dividends, after federal tax withholdings?", nonira_net)
  return zt

def ss_mdcare(ss=2422.00):
  """
   2024 SS monthly minus medicare with 20% fed tax
  """
  # mdcare = 148.50 + 59.40 + 12.30
  # mdcare = 297.00 # according to 2023 mysocialcecurity online benefits page
  mdcare = 559.00 # according to 2024 mysocialcecurity online benefits page
  ssmd =  0.8*(ss - mdcare)
  pp2("2024 Social Security before and after Medicare withholdings:", ss, mdcare, ssmd)
  return ssmd

#def mo_income(modiv, ss=2039.40*1.059):
def mo_income():
  """
  combined net monthly income after withhodlings
  """
  ssmd = ss_mdcare()
  # parms are 2024 monthly before withholdings
  pp2("\nMonthly social security payments, after medicare and federal tax withholdings?", ssmd)

  irad = {'fid':0.0, 'ml':0.0};  nonirad = {'zt':0.0}
  ira_net = ira_dividends(irad)
  pp2("\nMonthly IRA dividends, after federal tax withholdings?", ira_net)

  non_irad = {'zt':0.0}
  modiv = ira_dividends(irad) + nonira_dividends(non_irad)

  mo_income = modiv + ssmd 
  pp2("\nAnticipated total monthly income total after witholdings (SS + muni + other divs):",mo_income) 
  return mo_income
# end mo_income()

def main():
  pp2("My Social Security benefits: https://secure.ssa.gov/myCYB/start")
  pp2("Medicare premiums info: https://www.ssa.gov/benefits/medicare/medicare-premiums.html\n-----")

  all_assets = assets()
  pp2("Sum of All financial assets:", all_assets)

  ss2021 = 2039.40
  ss2022 = 1.059 * ss2021 
  ss2024 = 2422.00
  ssmd = ss_mdcare(ss2024)
  monthly = mo_income()
  pp2("Estimated total monthly income net after withholdings:", monthly)
# end main

if __name__ == '__main__':
  main()

