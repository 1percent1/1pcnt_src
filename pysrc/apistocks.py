import requests

url = "https://apistocks.p.rapidapi.com/daily"

querystring = {"symbol":"AAPL","dateStart":"2021-07-01","dateEnd":"2021-07-31"}

headers = {
	"X-RapidAPI-Key": "93e0b4a182msh69b5a7bf7ebfe85p1a919cjsnb1235f813606",
	"X-RapidAPI-Host": "apistocks.p.rapidapi.com"
}

response = requests.get(url, headers=headers, params=querystring)

print(response.json())
