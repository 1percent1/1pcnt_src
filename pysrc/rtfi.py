#!/usr/bin/env python3
import requests, sys

def rtfi_quote(ticker="googl"):
  """
  https://rapidapi.com/letscrape-6bRBa3QguO5/api/real-time-finance-data/
  """
  url = "https://real-time-finance-data.p.rapidapi.com/search"
  query = {"query":ticker, "language":"en"}
  header = {
    "X-RapidAPI-Key": "93e0b4a182msh69b5a7bf7ebfe85p1a919cjsnb1235f813606",
	"X-RapidAPI-Host": "real-time-finance-data.p.rapidapi.com"
  }

  print(header, query)

  resp = requests.get(url, headers=header, params=query)
  jquote = resp.json()
  print(jquote)
  return jquote

def main():
  """
  https://rapidapi.com/category/Finance -- try finance quote APIs listed bi rapidAPI
  """
  ticker = 'googl'
  if( len(sys.argv) > 1 ):
    ticker = sys.argv[1]

  jquote = rtfi_quote(ticker)
  print(jquote)

if __name__ == '__main__':
  main()

