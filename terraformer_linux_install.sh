#!/bin/bash
#from https://github.com/GoogleCloudPlatform/terraformer.git

export PROVIDER={all,google,aws,kubernetes}
curl -LO https://github.com/GoogleCloudPlatform/terraformer/releases/download/$(curl -s https://api.github.com/repos/GoogleCloudPlatform/terraformer/releases/latest | grep tag_name | cut -d '"' -f 4)/terraformer-${PROVIDER}-linux-amd64
chmod +x terraformer-${PROVIDER}-linux-amd64
sudo mv terraformer-${PROVIDER}-linux-amd64 /usr/local/bin/terraformer

# also terragrunt
# https://terragrunt.gruntwork.io/docs/getting-started/install/

# and docker / podman
# docker pull gruntwork/terragrunt
# podman pull gruntwork/terragrunt
