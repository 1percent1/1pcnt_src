Go:

https://blog.opstree.com/2020/04/14/create-your-own-container-using-linux-namespaces-part-1/
https://github.com/Deveshs23/mycontainer.git


---

C:

https://man7.org/linux/man-pages/man7/user_namespaces.7.html

       The program below is designed to allow experimenting with user
       namespaces, as well as other types of namespaces.  It creates
       namespaces as specified by command-line options and then executes
       a command inside those namespaces.  The comments and usage()
       function inside the program provide a full explanation of the
       program.  The following shell session demonstrates its use.

       First, we look at the run-time environment:

           $ uname -rs     # Need Linux 3.8 or later
           Linux 3.8.0
           $ id -u         # Running as unprivileged user
           1000
           $ id -g
           1000

       Now start a new shell in new user (-U), mount (-m), and PID (-p)
       namespaces, with user ID (-M) and group ID (-G) 1000 mapped to 0
       inside the user namespace:

           $ ./userns_child_exec -p -m -U -M '0 1000 1' -G '0 1000 1' bash

       The shell has PID 1, because it is the first process in the new
       PID namespace:

           bash$ echo $$
           1

       Mounting a new /proc filesystem and listing all of the processes
       visible in the new PID namespace shows that the shell can't see
       any processes outside the PID namespace:

           bash$ mount -t proc proc /proc
           bash$ ps ax
             PID TTY      STAT   TIME COMMAND
               1 pts/3    S      0:00 bash
              22 pts/3    R+     0:00 ps ax

       Inside the user namespace, the shell has user and group ID 0, and
       a full set of permitted and effective capabilities:

           bash$ cat /proc/$$/status | egrep '^[UG]id'
           Uid: 0    0    0    0
           Gid: 0    0    0    0
           bash$ cat /proc/$$/status | egrep '^Cap(Prm|Inh|Eff)'
           CapInh:   0000000000000000
           CapPrm:   0000001fffffffff
           CapEff:   0000001fffffffff


---

SEE ALSO         top
       newgidmap(1), newuidmap(1), clone(2), ptrace(2), setns(2),
       unshare(2), proc(5), subgid(5), subuid(5), capabilities(7),
       cgroup_namespaces(7), credentials(7), namespaces(7),
       pid_namespaces(7)

       The kernel source file
       Documentation/namespaces/resource-control.txt.
COLOPHON         top
       This page is part of release 5.11 of the Linux man-pages project.
       A description of the project, information about reporting bugs,
       and the latest version of this page, can be found at
       https://www.kernel.org/doc/man-pages/.

Linux                          2021-03-22             USER_NAMESPACES(7)
Pages that refer to this page: nsenter(1),  systemd-detect-virt(1),  unshare(1),  clone(2),  getgroups(2),  ioctl_ns(2),  keyctl(2),  seteuid(2),  setgid(2),  setns(2),  setresuid(2),  setreuid(2),  setuid(2),  unshare(2),  cap_get_file(3),  proc(5),  subgid(5),  subuid(5),  capabilities(7),  cgroup_namespaces(7),  cgroups(7),  credentials(7),  mount_namespaces(7),  namespaces(7),  network_namespaces(7),  pid_namespaces(7),  getcap(8),  setcap(8)

