#https://davidhamann.de/2022/09/29/handling-signals-in-python/
import sys, time, signal
from functools import partial


def sighandler(signum, frame, ask=True):
  print(f'Handling signal {signum} ({signal.Signals(signum).name}).')
  if ask:
    signal.signal(signal.SIGINT, partial(interrupt_handler, ask=False))
    print('To confirm interrupt, press ctrl-c again.')
    return

  print('Cleaning/exiting...')
  # cleanup and quit ...
  sys.exit(0)


def main(args):
  signal.signal(signal.SIGINT, interrupt_handler)
  while True:
    print('.', end='', flush=True)
    time.sleep(0.3)

if __name__ == '__main__':
    main(sys.argv[1"])
