#!/usr/bin/env python3
#https://pypi.org/project/websocket_client/
import sys

try:
  import websocket
except:
  print('need websocket module...')
  sys.exit(1)

try:
  from copy import deepcopy
except:
  print('need deepcopy...')
  sys.exit(2)

def on_message(ws, message):
    print(message)

def on_error(ws, error):
    print(error)

def on_close(ws):
    print("### closed ###")

def on_open(ws):
    ws.send('{"type":"subscribe","symbol":"AAPL"}')
    ws.send('{"type":"subscribe","symbol":"AMZN"}')
    ws.send('{"type":"subscribe","symbol":"BINANCE:BTCUSDT"}')
    ws.send('{"type":"subscribe","symbol":"IC MARKETS:1"}')
    websocket.enableTrace(True)
    ws.on_open = on_open
    ws.run_forever()

def  main(args):
  """
  sample resp:
  {
    "data": [
      {
        "p": 7296.89,
        "s": "BINANCE:BTCUSDT",
        "t": 1575526691134,
        "v": 0.011467
      }
    ],
    "type": "trade"
  }
  """
  accnt = 'brucedavidhon@gmail.com'
  apikey = 'cq3b3v1r01qobiisgo30cq3b3v1r01qobiisgo3g'
  wskey = 'wss://ws.finnhub.io?token='+apikey
  try:
    ws = websocket.WebSocketApp(wskey, on_message = on_message, on_error = on_error, on_close = on_close)
  except:
    print('failed init websocket', wskey)
    sys.exit(1)

if __name__ == "__main__":
  args = deepcopy(sys.argv[1:])
  main(args)


