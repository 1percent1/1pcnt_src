
https://tabulator.info/

https://github.com/holoviz/panel.git -- pandas dataframe presentation via tabulator

https://github.com/olifolkerd/tabulator.git

from Panel README:

Panel is an open-source Python library that lets you easily build powerful tools, dashboards and complex applications entirely in Python. It has a batteries-included philosophy, putting the PyData ecosystem, powerful data tables and much more at your fingertips. High-level reactive APIs and lower-level callback based APIs ensure you can quickly build exploratory applications, but you aren't limited if you build complex, multi-page apps with rich interactivity. Panel is a member of the HoloViz ecosystem, your gateway into a connected ecosystem of data exploration tools.

from tabulator README:

---

    <link href="dist/css/tabulator.min.css" rel="stylesheet">

    <script type="text/javascript" src="dist/js/tabulator.min.js"></script>

--- 

    <div id="example-table"></div>

--- 

    var table = new Tabulator("#example-table", {});

--- 

    bower install tabulator --save

    npm install tabulator-tables --save

